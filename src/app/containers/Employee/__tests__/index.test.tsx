import * as React from 'react';
import { render } from '@testing-library/react';

import { Employee } from '..';

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: str => str,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    };
  },
}));

describe('<Employee  />', () => {
  it('should match snapshot', () => {
    const loadingIndicator = render(<Employee />);
    expect(loadingIndicator.container.firstChild).toMatchSnapshot();
  });
});
