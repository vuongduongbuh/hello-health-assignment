import * as React from 'react';
import { render } from '@testing-library/react';

import { Table } from '..';

jest.mock('react-i18next', () => ({
  useTranslation: () => {
    return {
      t: str => str,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    };
  },
}));

describe('<Table  />', () => {
  it('should match snapshot', () => {
    const loadingIndicator = render(<Table />);
    expect(loadingIndicator.container.firstChild).toMatchSnapshot();
  });
});
