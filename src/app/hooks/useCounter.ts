import { useState } from 'react';

export function useCounter(intialCount = 0) {
  const [count, setCount] = useState(0);
  return [count, setCount];
}
